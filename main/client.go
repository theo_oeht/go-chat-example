package main

import (
	"log"

	"github.com/gorilla/websocket"
)

type client struct {
	socket *websocket.Conn
	send   chan []byte
	room   *room
}

func (c *client) read() {
	defer func() {
		log.Println("In defer read, close socket")
		c.socket.Close()
	}()

	for {
		_, msg, err := c.socket.ReadMessage()
		if err != nil {
			log.Println("read error")
			return
		}
		c.room.forward <- msg
	}
}

func (c *client) write() {
	defer func() {
		log.Println("In defer write, close socket")
		c.socket.Close()
	}()

	for msg := range c.send {
		err := c.socket.WriteMessage(websocket.TextMessage, msg)
		if err != nil {
			log.Println("write error")
			return
		}
	}
}
